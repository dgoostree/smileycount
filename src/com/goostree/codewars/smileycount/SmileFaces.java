package com.goostree.codewars.smileycount;

import java.util.List;

public class SmileFaces {
	public static final String regexp = "[:|;][-|~]?[)|D]";
	
	public static int countSmileys(List<String> arr) {
	      int count = 0;
	      
	      for(String current : arr) {
	    	  if(current.matches(regexp)){
	    		  count++;
	    	  }
	      }
	      
	      return count;
	}
}
